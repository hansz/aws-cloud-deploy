
FROM registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        yarn \
        wget \ 
        ca-certificates \
        python3-pip && \
    pip3 install cfn-lint && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y 

